# -*- coding: utf-8 -*-
"""
Created on Thu Dec 12 11:38:03 2019

@author: Simon Lohrmann

Es werden verschiedene Algorithmen zur Bestimmung von Minimalwerten von 
Funktionen getestet und die Funktionswerte nach 100 Iterationen bei
verschiedenen Seeds verglichen. Als Daten werden wieder die Weinsorten 
betrachtet.
"""


#get values
import pandas as pd
import numpy as np
import matplotlib as mlp
import matplotlib.pyplot as plt
from math import log
df = pd.read_csv('winequality-white.csv',sep=";")
b = df["quality"]
A = df.drop('quality', 1)
#define functions
f = [(lambda y: (lambda x:log(1+(np.dot(A.iloc[y],x)-b[y])**2)))(i) for i in range(len(A.index))]
gradients = [(lambda y: (lambda x: 2*A.iloc[y]*(np.dot(A.iloc[y],x)-b[y])/(1+(np.dot(A.iloc[y],x)-b[y])**2)))(i) for i in range(len(A.index))]
start=np.zeros(11)
    
def SGDseed(func, gradient, stepsize, x_new,seed):
    ran = np.random.RandomState(seed)
    n = len(gradient)
    m = 0
    while ((m < 100) ):
        m+=1
        i = ran.randint(n)
        dfi = gradient[i]
        x_new = x_new-stepsize*dfi(x_new)
    f_new=sum(func[i](x_new) for i in range(len(A.index)))
    return f_new

    
def SFRBGseed(func, gradients, stepsize, x_new, seed):
    ran = np.random.RandomState(seed)
    n = len(gradients)
    grad_new = x_new
    m=0
    while(m<100):
        m=m+1
        i = ran.randint(n)
        grad_old = grad_new
        grad_new = gradients[i](x_new)
        x_new = x_new-2*stepsize*grad_new+stepsize*grad_old
    f_new=sum(func[i](x_new) for i in range(len(A.index)))
    return f_new

def SFRBG_newGradseed(func, gradients, stepsize, x_new,seed):
    ran = np.random.RandomState(seed)
    n = len(gradients)
    x_old = x_new+1
    m=0
    while(m<100):
        m=m+1
        i = ran.randint(n)
        grad_old=gradients[i](x_old)
        grad_new = gradients[i](x_new)
        x_old = x_new
        x_new = x_new-2*stepsize*grad_new+stepsize*grad_old
    f_new=sum(func[i](x_new) for i in range(len(A.index)))
    return f_new

SGD=[]
SFRBG=[]
SFRBG_newGrad=[]
for i in range(100):
    f_SGD=SGDseed(f,gradients,10**-5,start,i)
    SGD.append(f_SGD)
    f_SFRBG=SFRBGseed(f,gradients,10**-5,start,i)
    SFRBG.append(f_SFRBG)
    f_SFRBG_newGrad=SFRBG_newGradseed(f,gradients,10**-5,start,i)
    SFRBG_newGrad.append(f_SFRBG_newGrad)
print(max(SGD),max(SFRBG),max(SFRBG_newGrad))
    