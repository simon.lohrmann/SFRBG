# -*- coding: utf-8 -*-
"""
Created on Mon Jun  3 09:24:02 2019

@author: Simon Lohrmann

Es werden verschiedene Algorithmen zur Bestimmung von Minimalwerten von 
Funktionen getestet und die Entwicklung der Funktionswerte geplottet.
Als Funktion wurde der mittlere quadratische Fehler von zufällig erstellten 
Punkten (A,b) gewählt. 
"""

import numpy as np
import matplotlib.pyplot as plt

def rando(n=10**5,start = 0, end = 1):##Zufällige Punkte bilden ca y=x
    x = np.linspace(start,end,n)
    ran = np.random.RandomState(1)
    rand = ran.uniform(-1,1,n)#Abweichung ist Gleichverteilungf auf [-1,1]
    y = x + rand
    return (x,y)#y=f(x) für die gesuchte Funktion
    
"""Die verschiedenen Algorithmen zum Lösen des Problems"""


"""Die folgende Funktion plotte tdie Entwicklung der Funktionswerte bei dem SGD
Als Variablen sind die zu minimierente Funktion f, deren Gradienten gradient,
eine vorgegebene Schrittweite stepsize und ein Startwert x_new.
Die Ausgabe nach Durchführung ist der gewollte Plot. 
Zudem wird der letzte Funktionswert zurückgegeben."""
def SGDplot(f, gradient, stepsize, x_new):
    ran = np.random.RandomState(2)
    n = len(gradient)
    lis = [f(x_new)]#Starte mit aktuellem Funktionswert
    m = 1
    while ((m < 100) ):#Führe 100 Iterationen durch
        m+=1
        i = ran.randint(n)#Wähle zufällig einen Gradienten f_i
        dfi = gradient[i]
        x_new = x_new-stepsize*dfi(x_new)#Aktualisiere nach Vorschrift des SGD
        lis.append(f(x_new))#Füge neuen Funktionswert hinzu
    plt.plot(range(len(lis)),lis,label="SGD")
    return x_new

"""Das Gradientenverfahren"""
def gradPlot(f,gradients,stepsize,x_new):
    lis = [f(x_new)]
    n=len(gradients)
    m=0
    while(m<100):
        m=m+1
        x_new = x_new-stepsize*sum([gradients[i](x_new) for i in range(len(gradients))])/n
        lis.append(f(x_new))
    plt.plot(range(len(lis)),lis,label="GV")
    return x_new
"""Plotte die Entwicklung des Funktionswertes vom FRB Verfahren.
Für die Variablen/Rückgabe siehe SGD"""
def FRBplot(f, gradients, step, x_new):
    grad_new = 0
    n=len(gradients)
    lis = [f(x_new)]
    m=0
    while(m<100):
        m=m+1
        grad_old = grad_new#alten Gradienten merken für die Iteration
        grad_new = sum([gradients[i](x_new) for i in  range(len(gradients))])/n
        x_new = x_new - step*2*grad_new + step*grad_old#x aktualisieren
        lis.append(f(x_new))
    plt.plot(range(len(lis)),lis,label="FRB")
    return x_new

"""Plotte die Entwicklung des SFRBG bei den ersten 100 Iterationen
Variablen und Rpckgabe ist gleich dem SGD"""
def SFRBGplot(f, gradients, stepsize, x_new):
    ran = np.random.RandomState(2)
    n = len(gradients)
    grad_new = 0
    lis = [f(x_new)]
    m=0
    while(m<100):
        m=m+1
        i = ran.randint(n)
        grad_old = grad_new
        grad_new = gradients[i](x_new)
        x_new = x_new-2*stepsize*grad_new+stepsize*grad_old
        lis.append(f(x_new))
    plt.plot(range(len(lis)),lis,label="SFRBG")
    plt.xlabel('Iteration')
    plt.ylabel('f(x)')
    return x_new

"""Plotte die Entwicklung des SFRBG mit neuem Gradienten bei den ersten 
100 Iterationen. Variablen und Rückgabe ist gleich dem SGD"""
def SFRBG_neuerGradplot(f, gradients, stepsize, x_new):
    ran = np.random.RandomState(2)
    n = len(gradients)
    grad_new = 0
    x_old = x_new+1
    lis = [f(x_new)]
    m=0
    while(m<100):
        m=m+1
        i = ran.randint(n)
        grad_old=gradients[i](x_old)
        grad_new = gradients[i](x_new)
        x_old = x_new
        x_new = x_new-2*stepsize*grad_new+stepsize*grad_old
        lis.append(f(x_new))
    plt.plot(range(len(lis)),lis,label="SFRBG_neuerGrad")
    return x_new

def SFRBG_gesamtplot(func, gradients, stepsize, x_new):
    ran = np.random.RandomState(2)
    n = len(gradients)
    x_old = 0
    lis = [func(x_new)]
    m=0
    while(m<100):
        m=m+1
        #berechne gesamten Gradienten
        grad=sum([gradients[i](x_new) for i in range(len(gradients))])/n
        #berechne einzelne Gradientenstücke für die Iteration
        i = ran.randint(n)
        grad_old=gradients[i](x_old)
        grad_new = gradients[i](x_new)
        #merke dir x für kommende Iteration
        x_old = x_new
        #aktualisiere x
        x_new = x_new-stepsize*(grad+grad_new-grad_old)
        lis.append(func(x_new))
    plt.plot(range(len(lis)),lis,label="SFRBG gesamter Gradient")    
    plt.xlabel('Iteration')
    plt.ylabel('f(x)')
    return x_new,lis[-1]


def main():
    #Punkte bilden, wo Annäherung gesucht ist
    A,b = rando()
    #Funktion und Gradient definieren
    f = lambda x: (sum([(0.5*(A[i]*x-b[i])**2) for i in range(A.size)])/A.size)
    gradients = [(lambda y: (lambda x: A[y]*(A[y]*x-b[y])))(i) for i in range(A.size)]
    #stochastische Algorithmen gemeinsam plotten, Schrittweite 0.1, Start:10
    x = SFRBGplot(f,gradients,10**-1,10)
    x = SFRBG_neuerGradplot(f,gradients,10**-1,10)
    x = SGDplot(f,gradients,10**-1,10)
    plt.legend(loc="upper right")
    plt.savefig("stochPlotsFct.pdf")
    plt.show()
    #Algorithmen plotten, die den gesamten Gradienten berechnen,Startwert: 15
    x = gradPlot(f,gradients,10**-1,10)
    x = FRBplot(f,gradients,10**-1,10)
    x = SFRBG_gesamtplot(f,gradients,10**-1,10)
    plt.legend(loc='upper right')
    plt.savefig("gesamtGradplots.pdf")
    plt.show()
main()