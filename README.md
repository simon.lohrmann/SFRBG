# SFRBG 
Dieses Repisotory beinhaltet die verschiedenen Programmiercodes für meine Bachelorarbeit zum Thema "Stochastische Varianten des Forward-Reflected-Backward Gradientenverfahren". 

Es werden auf verschiedenene Art und Weise Varianten des Gradientenverfahrens verglichen. Die Daten wurden häufig aus einem Datensatz über Weinqualitäten (https://archive.ics.uci.edu/ml/datasets/wine+quality) entnommen.
Möchte man einen andere Realisation einer Zufallsvariable testen, so kann man Random.State einen anderen Parameter geben.
Für eine andere Funktion lassen sich f/func und grad/gradient ändern. Die Gradienten sollen hierbei vorher bereits bestimmt worden sein. 
