# -*- coding: utf-8 -*-
"""
Created on Thu Dec 12 10:00:48 2019

@author: Simon Lohrmann

Ziel ist es, die verschiedenen Modelle auf 80% der Weine zu trainieren
und damit die Qualitäten der restlichen Weine vorherzusagen. Die Zielfunktion
ist die gleiche wie bei logreg.py.
Man sucht hierbei die Koeffizienten die den Fehler auf der Trainingsmenge 
minimieren und benutzt diese, um die Qualität auf der Testmenge zu schätzen.

"""

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from math import log
df = pd.read_csv('winequality-white.csv',sep=";")

start=np.zeros(11)
#Data Frame ist Trainingsset und Testset aufteilen
train, test = train_test_split(df, test_size=0.2)
b = train["quality"]
A = train.drop('quality', 1)
y_test=test["quality"]
A_test=test.drop('quality',1)
#Funktion und Gradient bestimmen
f = [(lambda y: (lambda x:log(1+(np.dot(A.iloc[y],x)-b.iloc[y])**2)))(i) for i in range(len(A.index))]
gradients = [(lambda y: (lambda x: 2*A.iloc[y]*(np.dot(A.iloc[y],x)-b.iloc[y])/(1+(np.dot(A.iloc[y],x)-b.iloc[y])**2)))(i) for i in range(len(A.index))]

"""Suche nach Minimum in den ersten 100 Iteration des SGD"""
def SGDmin(func, gradient, stepsize, x_new):
    ran = np.random.RandomState(10)
    f_min=sum(func[i](x_new) for i in range(len(A.index))) #Initialisiere Minimum als Startwert
    x_min=x_new
    n = len(gradient)
    m = 0
    while (m < 100):
        m+=1
        i = ran.randint(n)
        dfi = gradient[i]
        x_new = x_new-stepsize*dfi(x_new)
        f_new=sum(func[i](x_new) for i in range(len(A.index)))
        if(f_new<f_min):
            f_min=f_new
            x_min=x_new
    return x_min,f_new

"""Suche nach Minimum in den ersten 100 Iterationen des gewöhnlichen SFRBG"""
def SFRBGmin(func, gradients, stepsize, x_new):
    ran = np.random.RandomState(10)
    n = len(gradients)
    grad_new = x_new
    f_min=sum(func[i](x_new) for i in range(len(A.index)))
    x_min=x_new
    m=0
    while(m<100):
        m=m+1
        i = ran.randint(n)
        grad_old = grad_new
        grad_new = gradients[i](x_new)
        x_new = x_new-2*stepsize*grad_new+stepsize*grad_old
        f_new=sum(func[i](x_new) for i in range(len(A.index)))
        if(f_new<f_min):
            f_min=f_new
            x_min=x_new
    return x_min,f_min

"""Suche nach Minimum in den ersten 100 Iterationen des SFRBG mit neuem Gradienten
"""
def SFRBG_newGradmin(func, gradients, stepsize, x_new):
    ran = np.random.RandomState(10)
    n = len(gradients)
    x_old = x_new
    f_min=sum(func[i](x_new) for i in range(len(A.index)))
    x_min=x_new
    m=0
    while(m<100):
        m=m+1
        i = ran.randint(n)
        grad_old=gradients[i](x_old)
        grad_new = gradients[i](x_new)
        x_old = x_new
        x_new = x_new-2*stepsize*grad_new+stepsize*grad_old
        f_new=sum(func[i](x_new) for i in range(len(A.index)))
        if(f_new<f_min):
            f_min=f_new
            x_min=x_new
    return x_min,f_min
"""Funktion zur Vorhersage der Qualitäten durch gefundene Koeffizienten"""
def predict(coeffs):
    error=0.0
    for i in range(len(A_test.index)):
        pred= A_test.iloc[i].dot(coeffs).astype(float)
        val = y_test.iloc[i].astype(float)
        err = abs(pred-val)
        error += err
    return error 


def main():
    xSGD,fSGD=SGDmin(f,gradients,10**-5,start)
    error_SGD=predict(xSGD)
    xSFRB,fSFRB=SFRBGmin(f,gradients,10**-5,start)
    error_SFRBG=predict(xSFRB)
    xSFRB2,fSFRB2=SFRBG_newGradmin(f,gradients,10**-5,start)
    error_SFRBGnewG=predict(xSFRB2)
    print(error_SGD,error_SFRBG,error_SFRBGnewG)###vergleiche die Fehler
    
main()
   