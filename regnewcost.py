# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 10:40:21 2019

@author: Simon Lohrmann

Es werden verschiedene Algorithmen zur Bestimmung von Minimalwerten von 
Funktionen getestet und die Entwicklung der Funktionswerte geplottet.
Als zu minimierende Funktion wurde f(x)=(Ax-b)^2 gewählt, wobei
A die Matrix der Kovariablen und b der Zielvariablenvektor ist.
Als Daten wurde verschiedene Weine gewählt. Es soll deren Qualität geschätzt 
werden.
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
#den Datensatz als Data Frame speichern
df = pd.read_csv('winequality-white.csv',sep=";")
#den Zielvariablenvektor bestimmen
b = df["quality"]
#Die Kovariablenmatrix bilden
A = df.drop('quality', 1)
#Zielfunktion und deren Gradient bilden
m = len(A.index)
f = lambda x: sum((np.dot(A.iloc[i],x)-b[i])**2/m for i in range(m))
gradients = [(lambda y: (lambda x: 2*A.iloc[y]*(np.dot(A.iloc[y],x)-b[y])/m))(i) for i in range(len(A.index))]
#Startwert wählen
start=np.zeros(11)
    

"""Die verschiedenen Algorithmen, siehe random.py für mehr Infos"""
def SGDplot(func, gradient, stepsize, x_new):
    ran = np.random.RandomState(1)
    n = len(gradient)
    lis = [func(x_new)]
    m = 1
    while ((m < 100) ):
        m+=1
        i = ran.randint(n)
        dfi = gradient[i]
        x_new = x_new-stepsize*dfi(x_new)
        lis.append(func(x_new))
    plt.plot(range(len(lis)),lis,label="SGD")
    return x_new,lis[-1]
xSGD,fSGD=SGDplot(f,gradients,10**-2,start)
    
def SFRBplot(func, gradients, stepsize, x_new, tol = 10**-15):
    ran = np.random.RandomState(1)
    n = len(gradients)
    grad_new = x_new
    lis = [func(x_new)]
    m=0
    while(m<100):
        m=m+1
        i = ran.randint(n)
        grad_old = grad_new
        grad_new = gradients[i](x_new)
        x_new = x_new-2*stepsize*grad_new+stepsize*grad_old
        lis.append(func(x_new))
    plt.plot(range(len(lis)),lis,label="SFRBG")
    plt.xlabel('Iteration')
    plt.ylabel('f(x)')
    return x_new,lis[-1]
xSFRB,fSFRB=SFRBplot(f,gradients,10**-2,start)

def SFRB2plot(func, gradients, stepsize, x_new):
    ran = np.random.RandomState(1)
    n = len(gradients)
    x_old = x_new
    lis = [func(x_new)]
    m=0
    while(m<100):
        m=m+1
        i = ran.randint(n)
        grad_old=gradients[i](x_old)
        grad_new = gradients[i](x_new)
        x_old = x_new
        x_new = x_new-2*stepsize*grad_new+stepsize*grad_old
        lis.append(func(x_new))
    plt.plot(range(len(lis)),lis,label="SFRBG_neuerGradient")
    plt.legend(loc='upper right')
    return x_new,lis[-1]
xSFRB2,fSFRB2=SFRB2plot(f,gradients,10**-2,start)


plt.savefig("regnewcost.pdf")