# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 10:40:21 2019

@author: Simon Lohrmann

Es werden verschiedene Algorithmen zur Bestimmung von Minimalwerten von 
Funktionen getestet und die Entwicklung der Funktionswerte geplottet.
Als zu minimierende Funktion wurde f(x)=log(1+(Ax-b)^2) gewählt, wobei
A die Matrix der Kovariablen und b der Zielvariablenvektor ist.
Als Daten wurde verschiedene Weine gewählt. Es soll deren Qualität geschätzt 
werden.
Indem man RandomState ändert, lassen sich verschiedene Seeds durchführen.
"""

#get values
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from math import log
#den Datensatz als Data Frame speichern
df = pd.read_csv('winequality-white.csv',sep=";")
#den Zielvariablenvektor bestimmen
b = df["quality"]
#Die Kovariablenmatrix bilden
A = df.drop('quality', 1)
#Zielfunktion und deren Gradient bilden
f = [(lambda y: (lambda x:log(1+(np.dot(A.iloc[y],x)-b[y])**2)))(i) for i in range(len(A.index))]
gradients = [(lambda y: (lambda x: 2*A.iloc[y]*(np.dot(A.iloc[y],x)-b[y])/(1+(np.dot(A.iloc[y],x)-b[y])**2)))(i) for i in range(len(A.index))]
#Startwert wählen
start=np.zeros(11)

"""Die verschiedenen Algorithmen"""
def SGDplot(func, gradient, stepsize, x_new):
    ran = np.random.RandomState(10)
    n = len(gradient)
    lis = [sum(func[i](x_new) for i in range(len(A.index)))]
    m = 0
    while ((m < 100) ):
        m+=1
        i = ran.randint(n)
        dfi = gradient[i]
        x_new = x_new-stepsize*dfi(x_new)
        lis.append(sum(func[i](x_new) for i in range(len(A.index))))
    plt.plot(range(len(lis)),lis,label="SGD")
    return x_new,lis[-1]

    
def SFRBplot(func, gradients, stepsize, x_new, tol = 10**-15):
    ran = np.random.RandomState(10)
    n = len(gradients)
    grad_new = x_new
    lis = [sum(func[i](x_new) for i in range(len(A.index)))]
    m=0
    while(m<100):
        m=m+1
        i = ran.randint(n)
        grad_old = grad_new
        grad_new = gradients[i](x_new)
        x_new = x_new-2*stepsize*grad_new+stepsize*grad_old
        lis.append(sum(func[i](x_new) for i in range(len(A.index))))
    plt.plot(range(len(lis)),lis,label="SFRBG")
    plt.xlabel('Iteration')
    plt.ylabel('f(x)')
    return x_new,lis[-1]

def SFRB2plot(func, gradients, stepsize, x_new):
    ran = np.random.RandomState(10)
    n = len(gradients)
    x_old = x_new+1
    lis = [sum(func[i](x_new) for i in range(len(A.index)))]
    m=0
    while(m<100):
        m=m+1
        i = ran.randint(n)
        grad_old=gradients[i](x_old)
        grad_new = gradients[i](x_new)
        x_old = x_new
        x_new = x_new-2*stepsize*grad_new+stepsize*grad_old
        lis.append(sum(func[i](x_new) for i in range(len(A.index))))
    plt.plot(range(len(lis)),lis,label="SFRBG_neuerGradient")
    plt.legend(loc='upper right')
    return x_new,lis[-1]

xSGD,fSGD=SGDplot(f,gradients,10**-5,start)
xSFRB,fSFRB=SFRBplot(f,gradients,10**-5,start)
xSFRB2,fSFRB2=SFRB2plot(f,gradients,10**-5,start)



plt.savefig("SFRB2.pdf")